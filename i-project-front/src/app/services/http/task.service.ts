import { IpiTask } from './../../objects/ipi-task';
import { API_URL, HTTP_OPTIONS } from './../../utils/constants/global-constant';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) {}

  getAllTasks(): Observable<IpiTask[]> {
    return this.http.get<IpiTask[]>(`${API_URL}/tasks`, HTTP_OPTIONS);
  }

  getTaskById(id: number): Observable<IpiTask> {
    return this.http.get<IpiTask>(`${API_URL}/task/${id}`, HTTP_OPTIONS);
  }

  updateTaskById(id: number, task: IpiTask): Observable<IpiTask> {
    return this.http.put<IpiTask>(`${API_URL}/task/${id}`, task, HTTP_OPTIONS);
  }

  createTask(task: IpiTask): Observable<IpiTask> {
    return this.http.post<IpiTask>(`${API_URL}/task/create`, task, HTTP_OPTIONS);
  }

  deleteTask(id: number): Observable<string> {
    return this.http.delete<string>(`${API_URL}/task/${id}`, HTTP_OPTIONS);
  }
}
