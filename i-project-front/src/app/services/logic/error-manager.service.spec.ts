/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ErrorManagerService } from './error-manager.service';

describe('Service: ErrorManager', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ErrorManagerService]
    });
  });

  it('should ...', inject([ErrorManagerService], (service: ErrorManagerService) => {
    expect(service).toBeTruthy();
  }));
});
