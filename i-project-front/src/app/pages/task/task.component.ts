import { ErrorManagerService } from './../../services/logic/error-manager.service';
import { DisplayType } from './../../utils/enums/display-type.enum';
import { TaskService } from './../../services/http/task.service';
import { IpiTask } from './../../objects/ipi-task';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  displayType = DisplayType.UNDEFINED;
  displayTypeEnum = DisplayType;
  task: IpiTask | undefined;
  taskForm = new FormGroup(
    {
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    }
  );

  constructor(private activtedRoute: ActivatedRoute,
    private taskService: TaskService,
    private errorManagerService: ErrorManagerService) { }

  ngOnInit() {
    this.activtedRoute.paramMap.subscribe(params => { 
      const id = params.get('id') ? Number(params.get('id')) : -1;
      if (id != -1) {
        this.taskService.getTaskById(id).subscribe({
          next: (task: IpiTask) => { this.updateTask(task) },
          error: () => { this.manageError() }
        });
      } else {
        this.displayType = DisplayType.UNDEFINED;
        this.toggleFormInputsDisabled();
      }
    });
  }

  switchToUpdate(): void {
    this.displayType = DisplayType.UPDATE;
    this.toggleFormInputsDisabled();
  }

  switchToView(): void {
    this.displayType = DisplayType.VIEW;
    this.toggleFormInputsDisabled();
  }

  updateTask(task: IpiTask): void {
    if (task) {
      this.task = task;
      this.displayType = DisplayType.VIEW;
      this.initFormInputsValues();
    } else {
      this.displayType = DisplayType.UNDEFINED;
    }
    this.toggleFormInputsDisabled();
  }

  manageError(): void {
    this.errorManagerService.manageHttpError();
    this.displayType = DisplayType.UNDEFINED;
    this.toggleFormInputsDisabled();
  }

  onSubmit(): void {
    if (this.taskForm.valid) {
      const updatedTask = new IpiTask(undefined, this.taskForm.value.title, this.taskForm.value.description);
      this.taskService.updateTaskById(this.task?.id_task ? this.task.id_task : -1 , updatedTask).subscribe({
        next: (task: IpiTask) => { this.updateTask(task) },
        error: () => { this.manageError() }
      });
    }
  }

  toggleFormInputsDisabled(): void {
    if (this.displayType !== DisplayType.UPDATE) {
      this.taskForm.disable();
    } else {
      this.taskForm.enable();
    }
  }

  initFormInputsValues(): void {
    this.taskForm.setValue({
      title: this.task ? this.task.title : '',
      description: this.task ? this.task.description : ''
    });
  }

}
