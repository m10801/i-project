import { CreateTaskComponent } from './../../dialogs/create-task/create-task.component';
import { MatDialog } from '@angular/material/dialog';
import { ErrorManagerService } from './../../services/logic/error-manager.service';
import { IpiTask } from './../../objects/ipi-task';
import { TaskService } from './../../services/http/task.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-display-tasks',
  templateUrl: './display-tasks.component.html',
  styleUrls: ['./display-tasks.component.scss']
})
export class DisplayTasksComponent implements OnInit {

  tasks: IpiTask[] = [];

  constructor(
    public dialog: MatDialog,
    private taskService: TaskService,
    private manageErrorService: ErrorManagerService) { }

  ngOnInit() {
    this.updateTaskList();
  }

  setTasks(data: IpiTask[]): void {
    if (data) {
      this.tasks = data;
    }
  }

  displayError(): void {
    this.manageErrorService.manageHttpError();
  }

  displayCreatePopin(): void {
    const dialogRef = this.dialog.open(CreateTaskComponent, {
      width: '60%'
  });

    dialogRef.afterClosed().subscribe(result => {
      this.updateTaskList();
    });
  }

  deleteTask(id: number | undefined) {
    if (id || id === 0) {
      this.taskService.deleteTask(id).subscribe({
        next: (response: string) => { this.updateTaskList() },
        error: () => { this.displayError() }
      })
    }
  }

  updateTaskList() {
    this.taskService.getAllTasks().subscribe({
      next: (data: IpiTask[]) => { this.setTasks(data); },
      error: () => { this.displayError() }
    });
  }
}
