export class IpiTask {
  id_task: number | undefined;
  title: string;
  description: string;

  constructor(id_task: number | undefined, title: string, description: string) {
    if (id_task || id_task === 0) {
      this.id_task = id_task;
    }
    this.title = title;
    this.description = description;
  }
}
