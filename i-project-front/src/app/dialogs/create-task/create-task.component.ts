import { ErrorManagerService } from './../../services/logic/error-manager.service';
import { TaskService } from './../../services/http/task.service';
import { IpiTask } from './../../objects/ipi-task';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit {

  constructor(
    private taskService: TaskService,
    private errorManagerService: ErrorManagerService,
    private dialogRef: MatDialogRef<CreateTaskComponent>) { }

  taskForm = new FormGroup(
    {
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    }
  );

  ngOnInit() {
  }

  onSubmit() {
    if (this.taskForm.valid) {
      const createdTask = new IpiTask(undefined, this.taskForm.value.title, this.taskForm.value.description);
      this.taskService.createTask(createdTask).subscribe({
        next: (task: IpiTask) => { this.closeDialogAndSendTask(task) },
        error: () => { this.manageError() }
      });
    }
  }

  closeDialogAndSendTask(task: IpiTask) {
    if (task) {
      this.dialogRef.close(task);
    }
  }

  manageError() {
    this.errorManagerService.manageHttpError();
  }

}
